const webpack = require('webpack');
const merge = require('webpack-merge');
import {PATHS} from './PATHS.config';
const baseWebpackConfig = require('./webpack.base.config.babel');
const CopyWebpackPlugin = require('copy-webpack-plugin');
import ImageminWebpWebpackPlugin from 'imagemin-webp-webpack-plugin'

const devWebpackConfig = merge(baseWebpackConfig, {
    mode: 'development',
    devtool: 'cheap-module-eval-source-map',
    devServer: {
        contentBase: baseWebpackConfig.externals.paths.dist,
        port: 8082,
        overlay: {
            warnings: false,
            errors: true
        }
    },
    plugins: [
        new webpack.SourceMapDevToolPlugin({
            filename: '[file].map'
        }),
        new CopyWebpackPlugin([
            { from: `${PATHS.src.assets}/img`, to: `${PATHS.assets}img` },
            { from: `${PATHS.src.source}/static`, to: '' },
            { from: `${PATHS.src.assets}/fonts`, to: `${PATHS.assets}fonts` },
            { from: `${PATHS.src.source}/images`, to: `${PATHS.dist}/images` },
        ]),
        new ImageminWebpWebpackPlugin({
            config: [{
                test: /\.(jpe?g|png)/,
                options: {
                    quality: 100
                }
            }],
            overrideExtension: true,
            detailedLogs: false,
            strict: true
        })
    ]
});

module.exports = new Promise((resolve) => {
    resolve(devWebpackConfig)
});
