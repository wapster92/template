import {PATHS} from './PATHS.config';

const merge = require('webpack-merge');
const baseWebpackConfig = require('./webpack.base.config.babel');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const buildWebpackConfig = merge(baseWebpackConfig, {
    mode: 'production',
    plugins: [
        new CopyWebpackPlugin([
            { from: `${PATHS.src.source}/static`, to: '' },
            { from: `${PATHS.src.assets}/fonts`, to: `${PATHS.assets}fonts` },
        ])
    ]
});

module.exports = new Promise((resolve) => {
    resolve(buildWebpackConfig)
});