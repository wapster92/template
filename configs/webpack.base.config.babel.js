import {PATHS} from './PATHS.config';
import fs from 'fs';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';


const PAGES_DIR =  PATHS.src.source;
const PAGES = fs.readdirSync(PAGES_DIR).filter(fileName => fileName.endsWith('.html'));

module.exports = {
    externals: {
        paths: PATHS
    },
    entry: {
        app: `${PATHS.src.source}/index.js`
    },
    output: {
        filename: `${PATHS.assets}js/[name].js`,
        path: PATHS.dist,
        publicPath: ''
    },
    module: {
        rules: [{
            // JavaScript
            test: /\.js$/,
            loader: 'babel-loader',
            exclude: /node_modules/
        },
        {
            // SCSS/SASS
            test: /\.(scss|sass)$/,
            use: [
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: { sourceMap: true, url: false }
                },
                {
                    loader: 'postcss-loader',
                    options: { sourceMap: true, config: {path: 'configs/postcss.config.js'}}
                },
                {
                    loader: 'sass-loader',
                    options: { sourceMap: true }
                }
            ]
        },
        {
            // CSS
            test: /\.css$/,
            use: [
                'style-loader',
                MiniCssExtractPlugin.loader,
                {
                    loader: 'css-loader',
                    options: { sourceMap: true, url: false}
                },
                {
                    loader: 'postcss-loader',
                    options: { sourceMap: true, config: {path: './configs/postcss.config.js'}}
                }
            ]
        },
        /*{
            test: /\.(png|jpg|jpeg|gif|svg)$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]'
            }
        }*/]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: `${PATHS.assets}style/[name].css`
        }),
        ...PAGES.map(
            page =>
                new HtmlWebpackPlugin({
                    template: `${PAGES_DIR}/${page}`,
                    filename: `./${page}`,
                    inject: true,
                    chunks: ['app'],
                    minify: false
                })
        )
    ]
}