import path from 'path'

export const PATHS = {
    src: {
        source: path.join(__dirname, '../src'),
        assets: path.join(__dirname, '../src/assets'),
    },
    dist: path.join(__dirname, '../dist'),
    assets: 'assets/'
};