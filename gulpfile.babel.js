import {PATHS} from './configs/PATHS.config'
import gulp from 'gulp'
import svgsprite from 'gulp-svg-sprite'
import imagemin from 'gulp-imagemin'
import jpegrecompress from 'imagemin-jpeg-recompress'
import pngquant from 'imagemin-pngquant'
import gifsicle from 'imagemin-gifsicle'
import svgo from 'imagemin-svgo'
import webp from 'gulp-webp'
import spritesmith from 'gulp.spritesmith'
import merge from 'merge-stream'
import environments from 'gulp-environments'



const isProduction = environments.production;


gulp.task('svg:sprite', () => {
    return gulp.src(`${PATHS.src.assets}/img/sprites/*.svg`)
        .pipe(svgsprite({
                mode: {
                    stack: {
                        sprite: "../sprite.svg"
                    }
                }
            }
        ))
        .pipe(gulp.dest(`${PATHS.src.assets}/img`));
});


gulp.task('png:sprite',() => {
    let spriteData =
        gulp.src(`${PATHS.src.assets}/img/sprites/*.png`) // путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'spritepng.scss',
                cssFormat: 'scss',
                imgPath: '../img/sprite.png',
                retinaSrcFilter: `${PATHS.src.assets}/img/sprites/*@2x.png`,
                retinaImgName: 'sprite@2x.png',
                retinaImgPath: '../img/sprite@2x.png',
                algorithm: 'binary-tree'
            }));
    let imgStream =
        spriteData.img.pipe(gulp.dest(`${PATHS.src.assets}/img`)); // путь, куда сохраняем картинку
    let cssStream =
        spriteData.css.pipe(gulp.dest(`${PATHS.src.assets}/style`)); // путь, куда сохраняем стили

    return merge(imgStream, cssStream);
});


gulp.task('images:min', () => {
    return gulp.src(`${PATHS.src.source}/images/*.*`)
        .pipe(imagemin([
            gifsicle(),
            jpegrecompress({
                progressive: true,
                max: 90,
                min: 80
            }),
            pngquant(),
            svgo()
        ]))
        .pipe(gulp.dest(`${PATHS.dist}/images`))
});

gulp.task('images:webp', () => {
    return gulp.src(`${PATHS.src.source}/images/*.*`)
        .pipe(webp())
        .pipe(gulp.dest(`${PATHS.dist}/images`))
});

gulp.task('img:min', () => {
    return gulp.src([`${PATHS.src.source}/assets/img/*.*`, `!${PATHS.src.assets}/img/sprites/*.*`])
        .pipe(imagemin([
            gifsicle(),
            jpegrecompress({
                progressive: true,
                max: 90,
                min: 80
            }),
            pngquant(),
            svgo()
        ]))
        .pipe(gulp.dest(`${PATHS.dist}/assets/img`))
});

gulp.task('watch', () => {
    gulp.watch(`${PATHS.src.source}/assets/img/sprites/*.svg`, gulp.parallel('svg:sprite'));
    gulp.watch(`${PATHS.src.source}/assets/img/sprites/*.png`, gulp.parallel('png:sprite'));
});

if(isProduction()) {
    gulp.task('default', gulp.parallel(
        'png:sprite',
        'svg:sprite',
        'images:min',
        'images:webp',
        'img:min',
    ));
} else {
    gulp.task('default', gulp.parallel(
        'png:sprite',
        'svg:sprite',
        'watch'
    ));
}